var LOG = function () {
    this.index = [];
    window._log = console.log;
    window._dir = console.dir;
    window._warn = console.warn;
    window._error = console.error;
    
    this.providerDefault = 'local'; // cookie, cookieSession, localStorage, localSessionStorage
    this.providerHandler = {
        local: new LocalLogProvider(),
        remote: new RemoteLogProvider(),
    }
    this.listen();
};
LOG.prototype.listen = function () {
    var root = this;
    console.log = function () {
        return root.providerHandler[root.providerDefault].log(arguments);
    };
    console.dir = function () {
        return root.providerHandler[root.providerDefault].dir(arguments);
    };
    window.onerror = function () {
        return root.providerHandler[root.providerDefault].error(arguments);
    };
};
LOG.prototype.setProvider = function (provider) {
    this.providerDefault = provider;
};
LOG.prototype.write = function (titleArg, valueArg, provider) {
    var title, value;
    if (arguments.length == 1) {
        title = '';
        value = arguments[0];
    }
    if (arguments.length >= 2) {
        title = arguments[0];
        value = arguments[1];
    }
    return this.providerHandler[provider || this.providerDefault].write(title, value);
};
LOG.prototype.exist = function (key, value, experation, provider) {
    return this.providerHandler[provider || this.providerDefault].exist(key);
};
LOG.prototype.get = function (key, provider) {
    return this.providerHandler[provider || this.providerDefault].get(key);
};
LOG.prototype.getAll = function (provider) {
    return this.providerHandler[provider || this.providerDefault].getAll();
};
LOG.prototype.delete = function (key, provider) {
    return this.providerHandler[provider || this.providerDefault].delete(key);
};

