Object.prototype.gnexFindId = function (id) {
    return this.gnexFindByKey('id', id);
}
Object.prototype.gnexFindByKey = function (key, target) {
    var keys = Object.keys(this);
    for (var i = 0; i < keys.length; i++) {
        if (this[keys[i]][key] == target) {
            return {index: i, key: keys[i], value: this[keys[i]]};
        }
    }
    return null;
};