var GNEX = function () {
    this.modules = {
        'storage': STORAGE,
        'route': ROUTE,
        'log': LOG,
    };
};

GNEX.prototype.load = function () {
    var root = this;
    var args = Array.prototype.slice.call(arguments);
    console.dir(args);
    args.forEach(function (argument) {
        if (root[argument] !== undefined) return;
        root[argument] = new root.modules[argument]();
    });
};

window.gnex = new GNEX();
console.dir(window.gnex);