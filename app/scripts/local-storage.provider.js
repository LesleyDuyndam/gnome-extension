var LocalStorageProvider = function (type) {
    this.type = type || 'localStorage';
    this.index = this.getAll();
};

LocalStorageProvider.prototype.set = function (key, value) {
    window[this.type].setItem(key, value);
};
LocalStorageProvider.prototype.exists = function (key) {
    return undefined !== this.get(key);
};
LocalStorageProvider.prototype.getAll = function () {
    var storage = {};
    for ( var i = 0, len = localStorage.length; i < len; ++i ) {
        storage[localStorage.key(i)] = localStorage.getItem(localStorage.key(i));
    }
    return storage;
}
LocalStorageProvider.prototype.get = function (key) {
    return window[this.type].getItem(key);
};
LocalStorageProvider.prototype.delete = function (key) {
    return window[this.type].removeItem(key);
};