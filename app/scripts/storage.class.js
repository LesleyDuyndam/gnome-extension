var STORAGE = function () {
    this.providerDefault = 'cookie'; // cookie, cookieSession, localStorage, localSessionStorage
    this.providerHandler = {
        cookie: new CookieProvider(),
        localStorage: new LocalStorageProvider(),
        sessionStorage: new LocalStorageProvider('sessionStorage'),
    }
};
STORAGE.prototype.setProvider = function (provider) {
    this.providerDefault = provider;
}
STORAGE.prototype.set = function (key, value, experation, provider) {
    return this.providerHandler[provider || this.providerDefault].set(key, value, experation);
};
STORAGE.prototype.exist = function (key, value, experation, provider) {
    return this.providerHandler[provider || this.providerDefault].exist(key);
};
STORAGE.prototype.get = function (key, provider) {
    return this.providerHandler[provider || this.providerDefault].get(key);
};
STORAGE.prototype.getAll = function (provider) {
    return this.providerHandler[provider || this.providerDefault].getAll();
};
STORAGE.prototype.delete = function (key, provider) {
    return this.providerHandler[provider || this.providerDefault].delete(key);
};