var RemoteLogProvider = function () {
    this.index = this.getAll();
}
RemoteLogProvider.prototype.set = function (cname, cvalue, exdays) {
    var d = new Date();
    if (undefined == exdays) exdays = 365;
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    var expires = "expires=" + d.toUTCString();
    document.cookie = cname + "=" + cvalue + "; " + expires;
};
RemoteLogProvider.prototype.exists = function (key) {
    return undefined !== this.get(key);
};
RemoteLogProvider.prototype.getAll = function () {
    var cookies = { };
    if (document.cookie && document.cookie != '') {
        var split = document.cookie.split(';');
        for (var i = 0; i < split.length; i++) {
            var name_value = split[i].split("=");
            name_value[0] = name_value[0].replace(/^ /, '');
            cookies[decodeURIComponent(name_value[0])] = decodeURIComponent(name_value[1]);
        }
    }
    return cookies;
}
RemoteLogProvider.prototype.get = function (key) {
    var name = key + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
};
RemoteLogProvider.prototype.delete = function (key) {
    return undefined !== this.set(key, null, 0);
};