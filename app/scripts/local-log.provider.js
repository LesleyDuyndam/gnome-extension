var LocalLogProvider = function () {
    
}
LocalLogProvider.prototype.log = function () {
    return _log.apply(console, arguments[0]);
};
LocalLogProvider.prototype.dir = function () {
    return _dir.apply(console, arguments[0]);
};
LocalLogProvider.prototype.error = function () {
    return _error.apply(console, arguments[0]);
};
LocalLogProvider.prototype.write = function (title, value) {
    if (title && title !== '') {
        console.log('=== ' + title.toUpperCase() + " ===");
    }
    if (typeof value == 'string') {
        console.log(value);
    }
    if (typeof value == 'array') {
        console.dir(value);
    }
    if (typeof value == 'object') {
        console.dir(value);
    }
    console.log("\n");
};