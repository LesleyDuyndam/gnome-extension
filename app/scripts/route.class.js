var ROUTE = function () {
    this.data = window.location;
    console.dir(this);
    this.data.params = this.getRouteParams();
};
ROUTE.prototype.get = function (key) {
    return this.data[key];
}
ROUTE.prototype.getRouteParams = function () {
    if (window.location.search.length <= 0) return null;
    var response = {};
    var searchString = window.location.search.replace('?', '');
    var searchArray = searchString.split('&');
    searchArray.forEach(function (snippetString) {
        var snippetArray = snippetString.split('=');
        response[snippetArray[0]] = snippetArray[1];
    });
    if (Object.keys(response).length <= 0) return null;
    return response;
};