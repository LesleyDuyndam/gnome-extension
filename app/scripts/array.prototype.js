Array.prototype.gnexFind = function (target) {
    if (typeof target == 'string') {
        return this.gnexFindString(target);
    }
    if (typeof target == 'array') {
        return this.gnexFindArray(target);
    }
    if (typeof target == 'object') {
        return this.gnexFindObject(target);
    }
    if (typeof target == 'number') {
        return this.gnexFindNumber(target);
    }
};
Array.prototype.gnexFindString = function (target) {
    var i;
    for (i = 0; i < this.length; i++) {
        if (this[i] === target) {
            return {index: i, value: this[i]};
        }
    }
    return null;
};
Array.prototype.gnexFindArray = function (target) {
    for (var i = 0; i < this.length; i++) {
        if (JSON.stringify(this[i]) === JSON.stringify(target)) {
            return {index: i, value: this[i]};
        }
    }
    return null;
};
Array.prototype.gnexFindNumber = function (target) {
    var i;
    for (i = 0; i < this.length; i++) {
        if (parseInt(this[i]) === parseInt(target)) {
            return {index: i, value: this[i]};
        }
    }
    return null;
};
Array.prototype.gnexFindObject = function (target) {
    for (var i = 0; i < this.length; i++) {
        if (JSON.stringify(this[i]) === JSON.stringify(target)) {
            return {index: i, value: this[i]};
        }
    }
    return null;
};
Array.prototype.gnexFindId = function (id) {
    return this.gnexFindByKey('id', id);
};
Array.prototype.gnexFindByKey = function (key, target) {
    for (var i = 0; i < this.length; i++) {
        if (this[i][key] == target) {
            return {index: i, value: this[i]};
        }
    }
    return null;
};